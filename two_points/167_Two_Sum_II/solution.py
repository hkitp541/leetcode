class Solution(object):
    def twoSum(self, numbers, target):
        """
        :type numbers: List[int]
        :type target: int
        :rtype: List[int]
        """
        right_idx = len(numbers) - 1
        left_idx = 0
        while target != numbers[right_idx] + numbers[left_idx]:
            if target > numbers[right_idx] + numbers[left_idx]:
                left_idx += 1
            else:
                right_idx -= 1

        return [left_idx+1, right_idx+1]
