import math

class Solution(object):
    def judgeSquareSum(self, c):
        """
        :type c: int
        :rtype: bool
        """
        #return self._two_point_method(c)
        return self._fermat_thm(c)

    def _fermat_thm(self, c):
        for i in range(2, int(math.sqrt(c))+1):
            if c % i != 0:
                continue
            
            # i is factor of c
            cnt = 0
            while c % i == 0:
                cnt += 1
                c /= i
            
            if i % 4 == 3 and cnt % 2 == 1:
                return False
        
        return c % 4 != 3
    
    def _two_point_method(self, c):
        right = math.ceil(math.sqrt(c))
        left = 0

        ans = True
        add_res = math.pow(left,2) + math.pow(right,2)
        while add_res != c:
            if left > right:
                ans = False
                break
            elif add_res > c:
                right -= 1
            elif add_res < c:
                left += 1
            add_res = math.pow(left,2) + math.pow(right,2)
        return ans
        